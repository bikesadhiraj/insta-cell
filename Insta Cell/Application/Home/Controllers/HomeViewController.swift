//
//  HomeViewController.swift
//  Insta Cell
//
//  Created by John on 10.08.2020.
//  Copyright © 2020 com.example. All rights reserved.
//

import UIKit
import SnapKit

class HomeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var instaTableView : UITableView = UITableView()
    var posts = [PostData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let localData = self.readLocalFile(forName: "posts") {
            self.parse(jsonData: localData)
        }
        
        view.addSubview(instaTableView)
        
        setupInstaTableView()
        
        instaTableView.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide)
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
            make.bottom.equalTo(view.safeAreaLayoutGuide)
        }
    }
    
    private func setupInstaTableView() {
        instaTableView.translatesAutoresizingMaskIntoConstraints = false
        instaTableView.delegate = self
        instaTableView.dataSource = self
        instaTableView.tableFooterView = UIView(frame: .zero)
        instaTableView.register(InstaCell.self, forCellReuseIdentifier: "instaCell")
    }
    
    private func readLocalFile(forName name: String) -> Data? {
        do {
            let bundlePath = Bundle.main.path(forResource: name, ofType: "json")!
            let data = try Data(contentsOf: URL(fileURLWithPath: bundlePath))
            return data
            
        } catch {
            print(error)
        }
        return nil
    }
    
    private func parse(jsonData: Data) {
        do {
            guard let jsonResult = try? JSONSerialization.jsonObject(with: jsonData, options: []) else { return }
            let jsonArray = jsonResult as? [[String : Any]]
            
            let parsePosts : [PostData] = jsonArray!.compactMap { [weak self] in
                guard
                    let _ = self,
                    let userPhoto = $0["userphoto"] as? String,
                    let userName = $0["username"] as? String,
                    let placeName = $0["placename"] as? String,
                    let postPhotos = $0["postphotos"] as? [String],
                    let likedBy = $0["likedby"] as? String,
                    let postComment = $0["postcomment"] as? String,
                    let publicTime = $0["publictime"] as? String
                    else { return nil }
                
                let postsOne = PostData(userphoto: userPhoto, username: userName, placename: placeName, postphotos: postPhotos, likedby: likedBy, postcomment: postComment, publictime: publicTime)
                return postsOne
            }
            self.posts = parsePosts
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        posts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "instaCell") as? InstaCell
        let cellPost = self.posts[indexPath.row]
        cell?.postImages.removeAll()
        cell?.updateCellWith(userPhoto: cellPost.userphoto, userName: cellPost.username, placeName: cellPost.placename, postPhotos: cellPost.postphotos, likedBy: cellPost.likedby, postComment: cellPost.postcomment, publicTime: cellPost.publictime)
        return cell!
    }
    
}
