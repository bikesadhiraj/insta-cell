//
//  LikedByLabel.swift
//  Insta Cell
//
//  Created by John on 10.08.2020.
//  Copyright © 2020 com.example. All rights reserved.
//

import UIKit

class LikedByLabel : UILabel {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setupView()
    }
    
    func setupView() {
        self.font = UIFont(name: "HelveticaNeue", size: 14.0)
        self.textColor = .black
        self.text = "Liked by "
    }
    
}
