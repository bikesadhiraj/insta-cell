//
//  HeartButton.swift
//  Insta Cell
//
//  Created by John on 10.08.2020.
//  Copyright © 2020 com.example. All rights reserved.
//

import UIKit

class HeartButton : UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setupView()
    }
    
    func setupView() {
        let heartButtonImage = UIImage(named: "heart_button")
        self.setImage(heartButtonImage, for: .normal)
        self.tintColor = .lightGray
    }
    
}
