//
//  FlagButton.swift
//  Insta Cell
//
//  Created by John on 10.08.2020.
//  Copyright © 2020 com.example. All rights reserved.
//

import UIKit

class FlagButton : UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setupView()
    }
    
    func setupView() {
        let flagButtonImage = UIImage(named: "flag_button")
        self.setImage(flagButtonImage, for: .normal)
        self.tintColor = .lightGray
    }
    
}
