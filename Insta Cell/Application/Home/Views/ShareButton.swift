//
//  ShareButton.swift
//  Insta Cell
//
//  Created by John on 10.08.2020.
//  Copyright © 2020 com.example. All rights reserved.
//

import UIKit

class ShareButton : UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setupView()
    }
    
    func setupView() {
        let shareButtonImage = UIImage(named: "share_little_button")
        self.setImage(shareButtonImage, for: .normal)
        self.tintColor = .lightGray
    }
    
}
