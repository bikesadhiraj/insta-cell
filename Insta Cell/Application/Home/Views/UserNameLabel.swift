//
//  UserNameLabel.swift
//  Insta Cell
//
//  Created by John on 10.08.2020.
//  Copyright © 2020 com.example. All rights reserved.
//

import UIKit

class UserNameLabel : UILabel {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setupView()
    }
    
    func labelText(text: String) {
        self.text = text
    }
    
    func setupView() {
        self.font = UIFont(name: "HelveticaNeue-Bold", size: 14.0)
    }
    
}
