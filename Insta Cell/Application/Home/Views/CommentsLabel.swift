//
//  CommentsLabel.swift
//  Insta Cell
//
//  Created by John on 10.08.2020.
//  Copyright © 2020 com.example. All rights reserved.
//

import UIKit

class CommentsLabel : UILabel {
    
    let attrForUserName = [NSAttributedString.Key.font : UIFont(name: "HelveticaNeue-Bold", size: 14.0), NSAttributedString.Key.foregroundColor : UIColor.black]
    let attrForTextName = [NSAttributedString.Key.font : UIFont(name: "HelveticaNeue-Bold", size: 14.0), NSAttributedString.Key.foregroundColor : UIColor.systemBlue]
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setupView()
    }
    
    func labelText(username: String, text: String) {
        let userNameString = NSMutableAttributedString(string: username, attributes: attrForUserName as [NSAttributedString.Key : Any])
        
        let textString = NSMutableAttributedString(string: text, attributes: attrForTextName as [NSAttributedString.Key : Any])
        
        userNameString.append(textString)
        
        self.attributedText = userNameString
    }
    
    func setupView() {
        self.numberOfLines = 0
        self.lineBreakMode = .byWordWrapping
    }
    
}
