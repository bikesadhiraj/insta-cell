//
//  InstaCell.swift
//  Insta Cell
//
//  Created by John on 10.08.2020.
//  Copyright © 2020 com.example. All rights reserved.
//

import UIKit

class InstaCell : UITableViewCell {
    
    let topView : UIView = {
        let view = UIView()
        return view
    }()
    
    let scrollView : UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.showsHorizontalScrollIndicator = false
        return scrollView
    }()
    
    let imagesStackView : UIStackView = {
        let stackView = UIStackView()
        stackView.distribution = .equalSpacing
        return stackView
    }()
    
    let centerView : UIView = {
        let view = UIView()
        return view
    }()
    
    let bottomView : UIView = {
        let view = UIView()
        return view
    }()
    
    let andLabel = AndLabel()
    let andTextLabel = AndTextLabel()
    let commentsLabel = CommentsLabel()
    let flagButton = FlagButton()
    let heartButton = HeartButton()
    let likedByLabel = LikedByLabel()
    let likedByTextLabel = LikedByTextLabel()
    let moreButton = MoreButton()
    let placeNameLabel = PlaceNameLabel()
    let remarkButton = RemarkButton()
    let shareButton = ShareButton()
    let timeLabel = TimeLabel()
    let userNameLabel = UserNameLabel()
    
    var postImages = [UIImageView]()
    var imageName = [String]()
    
    lazy var pageControl : UIPageControl = {
        let pageControl = UIPageControl()
        pageControl.pageIndicatorTintColor = .lightGray
        pageControl.currentPageIndicatorTintColor = .gray
        pageControl.addTarget(self, action: #selector(pageControlTapped(sender:)), for: .valueChanged)
        return pageControl
    }()
    
    let accountImageView : UIImageView = {
        let imageView = UIImageView()
        return imageView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        addSubview(topView)
        addSubview(centerView)
        addSubview(bottomView)
        addSubview(commentsLabel)
        addSubview(timeLabel)
        
        topView.addSubview(accountImageView)
        topView.addSubview(userNameLabel)
        topView.addSubview(placeNameLabel)
        topView.addSubview(moreButton)
        
        addSubview(scrollView)
        scrollView.delegate = self
        scrollView.isPagingEnabled = true
        scrollView.addSubview(imagesStackView)
        
        centerView.addSubview(pageControl)
        centerView.addSubview(heartButton)
        centerView.addSubview(remarkButton)
        centerView.addSubview(shareButton)
        centerView.addSubview(flagButton)
        
        bottomView.addSubview(likedByLabel)
        bottomView.addSubview(likedByTextLabel)
        bottomView.addSubview(andLabel)
        bottomView.addSubview(andTextLabel)
        
        setupLayouts()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func updateCellWith(userPhoto: String, userName: String, placeName: String, postPhotos: [String], likedBy: String, postComment: String, publicTime: String) {
        accountImageView.image = UIImage(named: userPhoto)
        userNameLabel.labelText(text: userName)
        placeNameLabel.labelText(text: placeName)
        
        setPostPhotos(postPhotos: postPhotos)
        
        postImages.forEach { (view) in
            imagesStackView.addArrangedSubview(view)
            
            view.snp.makeConstraints { make in
                make.width.equalTo(self.scrollView.snp.width)
                make.height.equalTo(view.snp.height)
            }
        }
        
        pageControl.numberOfPages = postImages.count
        
        likedByTextLabel.labelText(text: likedBy)
        timeLabel.labelText(text: publicTime)
        commentsLabel.labelText(username: userName, text: postComment)
    }
    
    @objc func pageControlTapped(sender: UIPageControl) {
        let pageWidth = scrollView.bounds.width
        let offset = sender.currentPage * Int(pageWidth)
        UIView.animate(withDuration: 0.33, animations: { [weak self] in
            self?.scrollView.contentOffset.x = CGFloat(offset)
        })
    }
    
    func setPostPhotos(postPhotos: [String]) {
        for imageIndex in 0..<postPhotos.count {
            let imageView = UIImageView(image: UIImage(named: postPhotos[imageIndex]))
            self.postImages.append(imageView)
        }
    }
    
}
