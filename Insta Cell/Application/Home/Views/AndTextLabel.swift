//
//  AndTextLabel.swift
//  Insta Cell
//
//  Created by John on 10.08.2020.
//  Copyright © 2020 com.example. All rights reserved.
//

import UIKit

class AndTextLabel : UILabel {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setupView()
    }
    
    func setupView() {
        self.font = UIFont(name: "HelveticaNeue-Bold", size: 14.0)
        self.textColor = .black
        self.text = "94 others"
    }
    
}

