//
//  PostData.swift
//  Insta Cell
//
//  Created by John on 10.08.2020.
//  Copyright © 2020 com.example. All rights reserved.
//

import Foundation

struct PostData : Codable {
    
    var userphoto : String
    var username : String
    var placename : String
    var postphotos : [String]
    var likedby : String
    var postcomment : String
    var publictime : String
    
}
