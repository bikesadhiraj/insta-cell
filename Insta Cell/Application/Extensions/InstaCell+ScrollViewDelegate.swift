//
//  InstaCell+ScrollViewDelegate.swift
//  Insta Cell
//
//  Created by John on 10.08.2020.
//  Copyright © 2020 com.example. All rights reserved.
//

import UIKit

extension InstaCell: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageWidth = scrollView.bounds.width
        let pageFraction = scrollView.contentOffset.x/pageWidth
        
        pageControl.currentPage = Int((round(pageFraction)))
    }
    
}
