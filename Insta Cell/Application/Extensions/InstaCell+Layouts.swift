//
//  InstaCell+Layouts.swift
//  Insta Cell
//
//  Created by John on 10.08.2020.
//  Copyright © 2020 com.example. All rights reserved.
//

import UIKit

extension InstaCell {
    
    func setupLayouts() {
        
        topView.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
            make.height.equalTo(50.0)
        }
        
        accountImageView.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.leading.equalToSuperview().offset(10.0)
            make.height.equalTo(40.0)
            make.width.equalTo(40.0)
        }
        
        userNameLabel.snp.makeConstraints { make in
            make.bottom.equalTo(topView.snp.centerY)
            make.leading.equalTo(accountImageView.snp.trailing).offset(10.0)
            make.trailing.equalTo(moreButton.snp.leading)
        }
        
        placeNameLabel.snp.makeConstraints { make in
            make.top.equalTo(topView.snp.centerY)
            make.leading.equalTo(accountImageView.snp.trailing).offset(10.0)
            make.trailing.equalTo(moreButton.snp.leading)
        }
        
        moreButton.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.trailing.equalToSuperview().offset(-10.0)
            make.height.equalTo(40.0)
            make.width.equalTo(40.0)
        }
        
        scrollView.snp.makeConstraints { make in
            make.top.equalTo(topView.snp.bottom)
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
            make.height.equalTo(snp.width)
        }
        
        imagesStackView.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
            make.height.equalToSuperview()
        }
        
        centerView.snp.makeConstraints { make in
            make.top.equalTo(scrollView.snp.bottom)
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
            make.height.equalTo(40.0)
        }
        
        pageControl.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
        }
        
        heartButton.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(10.0)
            make.centerY.equalToSuperview()
            make.width.equalTo(30.0)
            make.height.equalTo(30.0)
        }
        
        remarkButton.snp.makeConstraints { make in
            make.leading.equalTo(heartButton.snp.trailing).offset(10.0)
            make.centerY.equalToSuperview()
            make.width.equalTo(30.0)
            make.height.equalTo(30.0)
        }
        
        shareButton.snp.makeConstraints { make in
            make.leading.equalTo(remarkButton.snp.trailing).offset(10.0)
            make.centerY.equalToSuperview()
            make.width.equalTo(30.0)
            make.height.equalTo(30.0)
        }
        
        flagButton.snp.makeConstraints { make in
            make.trailing.equalToSuperview().offset(-10.0)
            make.centerY.equalToSuperview()
            make.width.equalTo(30.0)
            make.height.equalTo(30.0)
        }
        
        bottomView.snp.makeConstraints { make in
            make.top.equalTo(centerView.snp.bottom).offset(10.0)
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
            make.height.equalTo(40.0)
        }
        
        likedByLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(10.0)
            make.bottom.equalTo(bottomView.snp.centerY)
        }
        
        likedByTextLabel.snp.makeConstraints { make in
            make.leading.equalTo(likedByLabel.snp.trailing)
            make.trailing.equalToSuperview()
            make.bottom.equalTo(bottomView.snp.centerY)
        }
        
        andLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(10.0)
            make.top.equalTo(bottomView.snp.centerY)
        }
        
        andTextLabel.snp.makeConstraints { make in
            make.leading.equalTo(andLabel.snp.trailing)
            make.trailing.equalToSuperview()
            make.top.equalTo(bottomView.snp.centerY)
        }
        
        commentsLabel.snp.makeConstraints { make in
            make.top.equalTo(bottomView.snp.bottom).offset(5.0)
            make.leading.equalToSuperview().offset(10.0)
            make.trailing.equalToSuperview().offset(-10.0)
        }
        
        timeLabel.snp.makeConstraints { make in
            make.top.equalTo(commentsLabel.snp.bottom).offset(5.0)
            make.leading.equalToSuperview().offset(10.0)
            make.trailing.equalToSuperview()
            make.bottom.equalToSuperview()
        }
        
    }
}
