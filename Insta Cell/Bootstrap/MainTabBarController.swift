//
//  ViewController.swift
//  Insta Cell
//
//  Created by John on 10.08.2020.
//  Copyright © 2020 com.example. All rights reserved.
//

import UIKit

class MainTabBarController: UITabBarController {
    
    let topImageView : UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "instagram_logo.png"))
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let leftNavigationButton : UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "photo_button_correct.png"), for: .normal)
        return button
    }()
    
    let rightNavigationButton : UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "share_button_correct.png"), for: .normal)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationBar()
        
        setTabBar()
        
        leftNavigationButton.addTarget(self, action: #selector(cameraTapped), for: .touchUpInside)
        rightNavigationButton.addTarget(self, action: #selector(shareTapped), for: .touchUpInside)
    }
    
    @objc func cameraTapped() {
        print("camera tapped")
    }
    
    func setNavigationBar() {
        UINavigationBar.appearance().backgroundColor = .white
        
        let leftNavigationItem = UIBarButtonItem(customView: leftNavigationButton)
        let rightNavigationItem = UIBarButtonItem(customView: rightNavigationButton)
        
        self.navigationItem.leftBarButtonItem = leftNavigationItem
        self.navigationItem.titleView = topImageView
        self.navigationItem.rightBarButtonItem = rightNavigationItem
    }
    
    func setTabBar() {
        let firstViewController = HomeViewController()
        let itemOne = UITabBarItem(title: "", image: UIImage(named: "home_button.png")?.withRenderingMode(.alwaysOriginal), tag: 0)
        firstViewController.tabBarItem = itemOne
        
        let secondViewController = SearchViewController()
        let itemTwo = UITabBarItem(title: "", image: UIImage(named: "search_button.png")?.withRenderingMode(.alwaysOriginal), tag: 1)
        secondViewController.tabBarItem = itemTwo
        
        let thirdViewController = AddViewController()
        let itemThree = UITabBarItem(title: "", image: UIImage(named: "add_button.png")?.withRenderingMode(.alwaysOriginal), tag: 2)
        thirdViewController.tabBarItem = itemThree
        
        let fourthViewController = LikesViewController()
        let itemFour = UITabBarItem(title: "", image: UIImage(named: "likes_button.png")?.withRenderingMode(.alwaysOriginal), tag: 3)
        fourthViewController.tabBarItem = itemFour
        
        let fifthViewController = ProfileViewController()
        let itemFive = UITabBarItem(title: "", image: UIImage(named: "profile_button.png")?.withRenderingMode(.alwaysOriginal), tag: 4)
        fifthViewController.tabBarItem = itemFive
        
        let tabBarList = [firstViewController, secondViewController, thirdViewController, fourthViewController, fifthViewController]
        
        viewControllers = tabBarList
        
        self.tabBar.backgroundColor = .lightGray
    }
    
    @objc func shareTapped() {
        print("share tapped")
    }
    
}

